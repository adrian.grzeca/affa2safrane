#include <Arduino.h>
#include <SoftWire.h>
#include <AsyncDelay.h>
#include <affa2p.h>
#include <TimerOne.h>

// Local globals
SoftWire *SW_I2C_WR;
uint8_t SDA_PIN;
uint8_t SCL_PIN;
uint8_t MRQ_PIN;
uint8_t MRQ_TIME_IN;
uint8_t MRQ_TIME_OUT;
uint8_t swI2CTxBuf[AFFA2P_I2C_BUF_SIZE];
uint8_t swI2CRxBuf[AFFA2P_I2C_BUF_SIZE];
uint8_t masterRequested;
uint8_t timerLastTask;

// Setup globals and enable MRQ
void AFFA2p_setup(uint8_t sdaPin, uint8_t sclPin, uint8_t mrqPin, uint8_t mrqTimeIn, uint8_t mrqTimeOut)
{
    // Assign globals
    SDA_PIN = sdaPin;
    SCL_PIN = sclPin;
    MRQ_PIN = mrqPin;
    MRQ_TIME_IN = mrqTimeIn;
    MRQ_TIME_OUT = mrqTimeOut;
    masterRequested = AFFA2P_NO;
    timerLastTask = AFFA2P_T_READ;

    // Setup pin
    pinMode(MRQ_PIN, INPUT_PULLUP);

    // Send ping and setup timer
    AFFA2P_send_ping();
    AFFA2p_init_LCD();
    Timer1.initialize(250000);
    Timer1.attachInterrupt(AFFA2p_timer_250ms);
}

// Waits for free I2C bus like an incel and request master status like incel
void AFFA2p_request_master_I2C()
{
    // Check if I2C is free
    while (digitalRead(MRQ_PIN) == LOW)
        delay(AFFA2P_DELAY); // Wait

    // Request master
    pinMode(MRQ_PIN, OUTPUT);
    digitalWrite(MRQ_PIN, LOW);
    delay(MRQ_TIME_IN); // Wait for display
    SW_I2C_WR = new SoftWire(SDA_PIN, SCL_PIN);
    SW_I2C_WR->begin();
    SW_I2C_WR->setTxBuffer(swI2CTxBuf, AFFA2P_I2C_BUF_SIZE);
    SW_I2C_WR->setRxBuffer(swI2CRxBuf, AFFA2P_I2C_BUF_SIZE);
    SW_I2C_WR->setTimeout(1000);
    SW_I2C_WR->setClock(AFFA2P_I2C_BUS_SPEED);

    // We are the master of I2C
    masterRequested = AFFA2P_YES;
}

// Frees up I2C bus
void AFFA2p_free_master_I2C()
{
    // End I2C transmision
    SW_I2C_WR->end();
    delete SW_I2C_WR;
    delay(MRQ_TIME_OUT); // Wait for display
    digitalWrite(MRQ_PIN, HIGH);
    pinMode(MRQ_PIN, INPUT_PULLUP);

    // We are not the master of I2C
    masterRequested = AFFA2P_NO;
}

// Timer handler
void AFFA2p_timer_250ms()
{
    if (timerLastTask == AFFA2P_T_READ)
    {
        AFFA2P_send_ping();
        timerLastTask = AFFA2P_T_WRITE;
    }
    else
    {
        // AFFA2P_recv_ping();
        timerLastTask = AFFA2P_T_READ;
    }
}

// Writes raw request to I2C buss
void AFFA2p_send_raw_I2C(const uint8_t *data, size_t len)
{
    uint8_t ret;

AFFA2p_send_raw_I2C_start:
    // Request master and check if is working
    AFFA2p_request_master_I2C();
    SW_I2C_WR->beginTransmission(0x23);

    for (size_t i = 0; i < len; i++)
        SW_I2C_WR->write(data[i]);

    ret = SW_I2C_WR->endTransmission();
    if (ret == 2 || ret == 3)
    {
        AFFA2p_free_master_I2C();
        delay(AFFA2P_DELAY);
        goto AFFA2p_send_raw_I2C_start;
    }

    AFFA2p_free_master_I2C();
}

// Send ping to LCD
void AFFA2P_send_ping()
{
    AFFA2p_send_raw_I2C(AFFA2P_PING_PACKET, AFFA2P_PING_PACKET[0] + 1);
}

// Receive ping from LCD
void AFFA2P_recv_ping()
{
    SW_I2C_WR->requestFrom(0x23, (uint8_t)2);

    Serial.print("Read:");
    while (SW_I2C_WR->available())
    {
        Serial.print(" 0x");
        Serial.print(SW_I2C_WR->read(), HEX);
    }
    Serial.println("");
}

// Initialize LCD
void AFFA2p_init_LCD()
{
    AFFA2p_send_raw_I2C(AFFA2P_INIT1_PACKET, AFFA2P_INIT1_PACKET[0] + 1);
    AFFA2p_send_raw_I2C(AFFA2P_INIT2_PACKET, AFFA2P_INIT2_PACKET[0] + 1);
    AFFA2p_send_raw_I2C(AFFA2P_INIT3_PACKET, AFFA2P_INIT3_PACKET[0] + 1);
}

// Update LCD
void AFFA2p_update_LCD(const char *text)
{
    // Variables
    char buf[17];
    int i, len;

    // Initialize LCD
    AFFA2p_init_LCD();

    // Check text length
    len = strlen(text);
    if (len > 8)
        len = 8; // Cut if it's longer

    for (i = 0; i < 8; i++)
        buf[0x08 + i] = 0x20; // Space
    for (i = 0; i < len; i++)
        buf[0x08 + i] = text[i]; // Copy char
    buf[16] = 0x00;              // NULL Char

    // Set rest params
    buf[0] = 0x10; // Message size
    buf[1] = 0x90; // Do not change
    buf[2] = 0x2F; // Do not change
    buf[3] = 0x00; // ??
    buf[4] = 0b00000000; // Icons #1 || DX | AF | ?? | TA | PT | ?? | ?? | TA || PT = PTY
    buf[5] = 0b00110000; // Icons #2 || ?? | ?? | U1 | U2 | DD | DD | DD | DD || DD are digit right to text; U1 and U2 are U1,U2,UAST icon (00 = U1, 01 = U2, 10 = UAST, 11 = Disable all icons!)
    buf[6] = 0x00; // Freq. of radio
    buf[7] = 0x00; // Freq. of radio

    // Send data to display
    AFFA2p_send_raw_I2C((uint8_t *)buf, 17);
}

// Gets who is master
uint8_t AFFA2p_MRQ_STATUS()
{
    if (masterRequested == AFFA2P_YES)
        return AFFA2P_BUS_MASTER_RADIO;
    else
    {
        if (digitalRead(MRQ_PIN) == LOW)
            return AFFA2P_BUS_MASTER_LCD;
        else
            return AFFA2P_BUS_MASTER_NOBODY;
    }
}
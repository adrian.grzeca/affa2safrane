#include <Arduino.h>
#include <SoftWire.h>
#include <stdint.h>
#include <affa2p.h>

// Settings
#define DEF_SDA_PIN 9
#define DEF_SCL_PIN 10
#define DEF_MRQ_PIN 8
#define DEF_MRQ_TIME_IN 4
#define DEF_MRQ_TIME_OUT 0
#define LOCAL_BUF_SIZE 64

// Globals
char buf[LOCAL_BUF_SIZE + 1];
char bufIndex;

void setup()
{
  // Setup
  Serial.begin(9600);
  Serial.println("Affa2Safrane\nHello");
  bufIndex = 0;

  // Test thing
  AFFA2p_setup(DEF_SDA_PIN, DEF_SCL_PIN, DEF_MRQ_PIN, DEF_MRQ_TIME_IN, DEF_MRQ_TIME_OUT);
  AFFA2p_update_LCD("Hello");
}

void loop()
{
  if (Serial.available() > 0)
  {
    uint8_t data = Serial.read();
    if (data >= 0x20)
    {
      if (bufIndex < LOCAL_BUF_SIZE)
      {
        buf[bufIndex++] = data;
        Serial.print((char)data);
      }
    }
    else if (data == 13)
    {
      buf[bufIndex++] = 0x00;
      AFFA2p_update_LCD(buf);
      bufIndex = 0;
      Serial.println("");
      Serial.println("Updating!!");
    }
  }
}
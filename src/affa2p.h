#ifndef __AFFA2P__
#define __AFFA2P__

#include <stdint.h>
#include <SoftWire.h>

// Static definitions
#define AFFA2P_I2C_BUS_SPEED 12000UL
#define AFFA2P_I2C_BUF_SIZE 32
#define AFFA2P_DELAY 2
#define AFFA2P_BUS_MASTER_NOBODY 0
#define AFFA2P_BUS_MASTER_LCD 1
#define AFFA2P_BUS_MASTER_RADIO 2
#define AFFA2P_NO 0
#define AFFA2P_YES 1
#define AFFA2P_T_READ 0
#define AFFA2P_T_WRITE 1

const uint8_t AFFA2P_PING_PACKET[] = {0x01, 0x11};
const uint8_t AFFA2P_INIT1_PACKET[] = {0x03, 0x90, 0x01, 0x60};
const uint8_t AFFA2P_INIT2_PACKET[] = {0x03, 0x90, 0x81, 0x02};
const uint8_t AFFA2P_INIT3_PACKET[] = {0x04, 0x90, 0x13, 0x00, 0x00}; // ostatni bajt 0x40 to LD, 0x20 to HI TEMP

// Funtions
void AFFA2p_setup(uint8_t sdaPin, uint8_t sclPin, uint8_t mrqPin, uint8_t mrqTimeIn, uint8_t mrqTimeOut);
void AFFA2p_request_master_I2C();
void AFFA2p_send_raw_I2C(const uint8_t *data, size_t len);
void AFFA2p_free_master_I2C();
void AFFA2p_timer_250ms();
void AFFA2P_send_ping();
void AFFA2P_recv_ping();
void AFFA2p_init_LCD();
void AFFA2p_update_LCD(const char *text);
uint8_t AFFA2p_MRQ_STATUS();

#endif // __AFFA2P__
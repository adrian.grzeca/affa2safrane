**AFFA2SAFRANE**

This is an Arduino Mega 2560 project to interface with the Renault AFFA2+ radio display. Currently only displaying works so it is just proof of concept. It also works with Carminat 6000 navigation because it uses the same protocol like AFFA2+ display.
 
It is for AFFA2+ not AFFA2++!! One plus not two pluses. Those are different displays.
 
If you want to contribute please let me know.

